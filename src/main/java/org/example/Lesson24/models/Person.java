package org.example.Lesson24.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import javax.validation.constraints.Min;
import javax.validation.constraints.Email;

public class Person {
    private int id;

    /**
     * @NotEmpty - запрещает вводить пустое значение: null или " ".
     * В случае ввода пустого значения выводит содержимое message
     */
    @NotEmpty(message = "Name should not be empty")
    /**
     * @Size - ограничиват длину имени от 2 до 30 символов
     * В случае выхода за диапазон выводит содержимое message
     */
    @Size(min = 2, max = 30, message = "Name should be between 2 and 30 characters")
    private String name;

    /**
     * @Min- устанавливает минимальное значение возраста равным нулю
     * В случае ввода отрицательных чисел выводит содержимое message
     */
    @Min(value = 0, message = "Age should be greater then 0")
    private int age;

    @NotEmpty(message = "Email should not be empty")
    /**
     * @Email - проверяет что в строке private String email лежит именно email
     * В случае ввода невалидного email выводит содержимое message
     */
    @Email(message = "Email should be valid")
    private String email;

    /**
     * пустой конструктор
     */
    public Person() {
    }

    /**
     * Конструктор с параметрами
     * @param id
     * @param name
     * @param age
     * @param email
     */

    public Person(int id, String name, int age, String email) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
